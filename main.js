// Global variables
var r, g, b, r_end, g_end, b_end, r_canvas, g_canvas, b_canvas, growthSpeed, sizeMultiplierSliderData, dirTendency, inputParams,
	initialSegmentColor = {r: 0, g: 0, b: 0},
	endSegmentColor = {r: 0, g: 0, b: 0},
    canvasBGColor = {r: 0, g: 0, b: 0},
	endColorStepChange = {r: 0, g: 0, b: 0},
	stepChangeColor = {r: 0, g: 0, b: 0},
	numberOfSegments = 0,
	initialSegmentSize, segmentSizeBoundariesSliderData, specialSizeChange, sizeProportion,
	maxNumberOfIterations, maxNumberOfSegments, 
	useColorStep, useGenerationRatioForAngle, useGenerationRatioForSize,
	sizeMultiplierMin, sizeMultiplierMax, numberOfChildrenSliderData, childAngleDiffSliderData,
	numberOfChildrenMin, numberOfChildrenMax,
	childAngleDiffMin, childAngleDiffMax, deflectionAngle,
	segmentSizeMin, segmentSizeMax,
	canvasWidth, canvasHeight, canvasInitPositionX, canvasInitPositionY;


// Extend usage of localStorage to hold JS objects in JSON
Storage.prototype.setObject = function(key, value) {
	this.setItem(key, JSON.stringify(value));
};
Storage.prototype.getObject = function(key) {
	var value = this.getItem(key);
	return value && JSON.parse(value);
};
	
function Color(r, g, b) {
	this.r = r;
	this.g = g;
	this.b = b;
}
Color.prototype.toString = function () {
	return "rgb(" + this.r + "," + this.g + "," + this.b + ")";
};
Color.prototype.add = function (dr, dg, db) {
	var r = this.r + dr;
	var g = this.g + dg;
	var b = this.b + db;
	
	//return new Color(Math.min(r, 0xFF), Math.min(g, 0xFF), Math.min(b, 0xFF));
	return new Color(r < 0 ? 0 : r > 255 ? 255 : r
					, g < 0 ? 0 : g > 255 ? 255 : g
					, b < 0 ? 0 : b > 255 ? 255 : b);
};

function Segment(angle, size, color, generation) {
	this.angle = angle;
	this.size = size;
	this.width = Math.max(1, size / sizeProportion);
	this.color = color;
	this.generation = generation + 1;
	this.childs = [];
	this.params = {x: 0, y: 0, a: 0};
}
Segment.prototype.draw = function (ctx, x, y, angle) {
	var a = this.angle + angle;
	var x0 = x + this.size * Math.cos(a);
	var y0 = y - this.size * Math.sin(a);
	x1 = Math.round(x);
	y1 = Math.round(y);
	x2 = Math.round(x0);
	y2 = Math.round(y0);
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.strokeStyle = this.color.toString();
	ctx.lineWidth = this.width;
	ctx.stroke();
	for (var i in this.childs) {
		var child = this.childs[i];
		child.draw(ctx, x0, y0, a);
	}
};
Segment.prototype.drawSegment = function (ctx) {
	var a = this.angle + this.params.a,
		x0 = this.params.x + this.size * Math.cos(a),
		y0 = this.params.y - this.size * Math.sin(a),
		x1 = Math.round(this.params.x),
		y1 = Math.round(this.params.y),
		x2 = Math.round(x0),
		y2 = Math.round(y0);
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.strokeStyle = this.color.toString();
	ctx.lineWidth = this.width;
	ctx.stroke();

	return {
		x: x0,
		y: y0,
		a: a
	};
};
function growInTime(ctx, segments) {
	var siblings = [];
	for (var s in segments) {
		var defaultParams = segments[s].drawSegment(ctx);
		for (var i in segments[s].childs) {
			segments[s].childs[i].params = defaultParams;
			// Put each child into siblings array
			siblings.push(segments[s].childs[i]);
		}
	}
	if (siblings.length != 0) {
		setTimeout(growInTime.bind(null, ctx, siblings), growthSpeed);
	}
}

function generate(s) {
	if (s.size >= segmentSizeMin && s.size <= segmentSizeMax && s.generation < maxNumberOfIterations) {
		var r = useColorStep ? stepChangeColor.r : endColorStepChange.r,
			g = useColorStep ? stepChangeColor.g : endColorStepChange.g,
			b = useColorStep ? stepChangeColor.b : endColorStepChange.b,
			color = s.color.add(r, g, b),
			numberOfChildren = getRandomInteger(numberOfChildrenMin, numberOfChildrenMax),
			generationRatio = s.generation / maxNumberOfIterations,
			directionTendency = 0,
			sizeMutiplier, childAngleDiff, deflection;
		
		// Define direction tendency for whole generation
		if (dirTendency == 1) {
			directionTendency = 0.5;
		}
		else if (dirTendency == 2) {
			directionTendency = - 0.5;
		}
		else if (dirTendency == 3) {
			directionTendency = Math.random() - 0.5;
		}

		for (var i = 1; i <= numberOfChildren; i++) {
			sizeMutiplier = getRandomInteger(sizeMultiplierMin, sizeMultiplierMax) / 100;
			childAngleDiff = Math.PI * getRandomInteger(childAngleDiffMin, childAngleDiffMax) / 100;
			deflection = deflectionAngle * 2 * Math.PI / 180;

            // Check using Generation Ratio for Angle
			if (useGenerationRatioForAngle == 1) {
				childAngleDiff *= generationRatio;	// from 0 to 1
			}
			else if (useGenerationRatioForAngle == 2) {
				childAngleDiff *= 1 - generationRatio;	// from 1 to 0
			}
            // Check using Generation Ratio for Size
			if (useGenerationRatioForSize == 1) {
				sizeMutiplier *= generationRatio;	// from 0 to 1
			}
			else if (useGenerationRatioForSize == 2) {
				sizeMutiplier *= 1 - generationRatio;	// from 1 to 0
			}
			// Change angle according to number of child
			if (i % 2 == 0) { 
				childAngleDiff *= -1; 
			} 
			else if (i % 3 == 0) { 
				childAngleDiff = 0;
				sizeMutiplier += specialSizeChange;
			}
			if (i > 3) {
			    childAngleDiff *= 1 / (i / 2);
            }
			
			if (dirTendency == 4) {	// to make for each child random tendency
				directionTendency = Math.random() - 0.5;
			}
			
			// Generate Child
			var s1 = new Segment(childAngleDiff + deflection * directionTendency, sizeMutiplier * s.size, color, s.generation);
			
			generate(s1);
			s.childs.push(s1);
		}
	}
}

function xload() {
	loadGlobalParams($('#inputPanel'));
    $('#canvas').attr('width', canvasWidth).attr('height', canvasHeight);

	var canvas = document.getElementById('canvas'),
        ctx = canvas.getContext('2d'),
		s = new Segment(Math.PI / 2, initialSegmentSize, 
						new Color(initialSegmentColor.r, initialSegmentColor.g, initialSegmentColor.b), 0);
		
	ctx.fillStyle = 'rgb(' + canvasBGColor.r + ', ' + canvasBGColor.g + ', ' + canvasBGColor.b + ')';
	ctx.fillRect(0, 0, canvasWidth, canvasHeight);
	generate(s);
	
	// TODO: implementovat podminku numberOfSegments < maxNumberOfSegments
	if (growthSpeed === 0) {
		s.draw(ctx, canvasInitPositionX, canvasInitPositionY, 0);
	}
	else {
		// Define default position, vector
		s.params.x = canvasInitPositionX;
		s.params.y = canvasInitPositionY;
		s.params.a = 0;
		// Set recursive function
		growInTime(ctx, [s]);
	}
	
}

function loadGlobalParams($inputPanel) {
	initialSegmentColor.r      = parseInt($inputPanel.find('#R').val(), 10) || $inputPanel.find('#R').data('sliderValue');
	initialSegmentColor.g      = parseInt($inputPanel.find('#G').val(), 10) || $inputPanel.find('#G').data('sliderValue');
	initialSegmentColor.b      = parseInt($inputPanel.find('#B').val(), 10) || $inputPanel.find('#B').data('sliderValue');
	endSegmentColor.r          = parseInt($inputPanel.find('#R_end').val(), 10) || $inputPanel.find('#R_end').data('sliderValue');
	endSegmentColor.g          = parseInt($inputPanel.find('#G_end').val(), 10) || $inputPanel.find('#G_end').data('sliderValue');
	endSegmentColor.b          = parseInt($inputPanel.find('#B_end').val(), 10) || $inputPanel.find('#B_end').data('sliderValue');
    canvasBGColor.r            = parseInt($inputPanel.find('#R_canvas').val(), 10) || $inputPanel.find('#R_canvas').data('sliderValue');
    canvasBGColor.g            = parseInt($inputPanel.find('#G_canvas').val(), 10) || $inputPanel.find('#G_canvas').data('sliderValue');
    canvasBGColor.b            = parseInt($inputPanel.find('#B_canvas').val(), 10) || $inputPanel.find('#B_canvas').data('sliderValue');
	stepChangeColor.r          = parseInt($inputPanel.find('#colorRStep').val(), 10);
	stepChangeColor.g          = parseInt($inputPanel.find('#colorGStep').val(), 10);
	stepChangeColor.b          = parseInt($inputPanel.find('#colorBStep').val(), 10);

	initialSegmentSize         = $inputPanel.find('#initialSegmentSize').val();
	specialSizeChange          = parseFloat($inputPanel.find('#childSizeSpecialChange').val());
    sizeProportion             = parseInt($inputPanel.find('#sizeProportion').val(), 10);
	maxNumberOfIterations      = parseInt($inputPanel.find('#maxNumberOfIterations').val(), 10);
	maxNumberOfSegments        = parseInt($inputPanel.find('#maxNumberOfSegments').val(), 10);
	growthSpeed                = parseInt($inputPanel.find('#growthSpeed').val(), 10);

	segmentSizeBoundariesSliderData = $inputPanel.find('#segmentSizeBoundaries').data();
	segmentSizeMin          = segmentSizeBoundariesSliderData.slider && segmentSizeBoundariesSliderData.slider._state.value[0] || segmentSizeBoundariesSliderData.sliderValue[0];
	segmentSizeMax          = segmentSizeBoundariesSliderData.slider && segmentSizeBoundariesSliderData.slider._state.value[1] || segmentSizeBoundariesSliderData.sliderValue[1];

	sizeMultiplierSliderData   = $inputPanel.find('#childSizeMultiplier').data();
	sizeMultiplierMin          = sizeMultiplierSliderData.slider && sizeMultiplierSliderData.slider._state.value[0] || sizeMultiplierSliderData.sliderValue[0];
	sizeMultiplierMax          = sizeMultiplierSliderData.slider && sizeMultiplierSliderData.slider._state.value[1] || sizeMultiplierSliderData.sliderValue[1];

	numberOfChildrenSliderData = $inputPanel.find('#numberOfChildren').data();
	numberOfChildrenMin        = numberOfChildrenSliderData.slider && numberOfChildrenSliderData.slider._state.value[0] || numberOfChildrenSliderData.sliderValue[0];
	numberOfChildrenMax        = numberOfChildrenSliderData.slider && numberOfChildrenSliderData.slider._state.value[1] || numberOfChildrenSliderData.sliderValue[1];

	childAngleDiffSliderData   = $inputPanel.find('#childAngleDiff').data();
	childAngleDiffMin          = childAngleDiffSliderData.slider && childAngleDiffSliderData.slider._state.value[0] || childAngleDiffSliderData.sliderValue[0];
	childAngleDiffMax          = childAngleDiffSliderData.slider && childAngleDiffSliderData.slider._state.value[1] || childAngleDiffSliderData.sliderValue[1];
	
	dirTendency                = $inputPanel.find('#dirTendency').val();
	deflectionAngle            = parseFloat($inputPanel.find('#deflectionAngle').val());
	useGenerationRatioForAngle = $inputPanel.find('#useGenerationRatioForAngle').val();
	useGenerationRatioForSize  = $inputPanel.find('#useGenerationRatioForSize').val();

    useColorStep               = $inputPanel.find('#useColorStep').is(":checked");
	endColorStepChange.r       = getEndColorStepChange("r");
	endColorStepChange.g       = getEndColorStepChange("g");
	endColorStepChange.b       = getEndColorStepChange("b");

	canvasWidth = parseInt($inputPanel.find('#canvasWidth').val(), 10);
	canvasHeight = parseInt($inputPanel.find('#canvasHeight').val(), 10);
	canvasInitPositionX = parseInt($inputPanel.find('#canvasInitPositionX').val(), 10);
	canvasInitPositionY = parseInt($inputPanel.find('#canvasInitPositionY').val(), 10);

	// Define object inputParams for JSON conversion and it data-set storage
	inputParams = {
		initialSegmentColor: {
			r: initialSegmentColor.r,
			g: initialSegmentColor.g,
			b: initialSegmentColor.b
		},
		endSegmentColor: {
			r: endSegmentColor.r,
			g: endSegmentColor.g,
			b: endSegmentColor.b
		},
        canvasBGColor: {
            r: canvasBGColor.r,
            g: canvasBGColor.g,
            b: canvasBGColor.b
        },
		stepChangeColor: {
			r: stepChangeColor.r,
			g: stepChangeColor.g,
			b: stepChangeColor.b
		},
		endColorStepChange: {
			r: endColorStepChange.r,
			g: endColorStepChange.g,
			b: endColorStepChange.b
		},
		initialSegmentSize: initialSegmentSize,
		segmentSizeMin: segmentSizeMin,
		segmentSizeMax: segmentSizeMax,
		specialSizeChange: specialSizeChange,
        sizeProportion: sizeProportion,
		maxNumberOfIterations: maxNumberOfIterations,
		maxNumberOfSegments: maxNumberOfSegments,
		sizeMultiplierMin: sizeMultiplierMin,
		sizeMultiplierMax: sizeMultiplierMax,
		numberOfChildrenMin: numberOfChildrenMin,
		numberOfChildrenMax: numberOfChildrenMax,
		childAngleDiffMin: childAngleDiffMin,
		childAngleDiffMax: childAngleDiffMax,
		deflectionAngle: deflectionAngle,
		useColorStep: useColorStep,
		useGenerationRatioForAngle: useGenerationRatioForAngle,
		useGenerationRatioForSize: useGenerationRatioForSize,
		growthSpeed: growthSpeed,
		dirTendency: dirTendency,
        canvasWidth: canvasWidth,
        canvasHeight: canvasHeight,
        canvasInitPositionX: canvasInitPositionX,
        canvasInitPositionY: canvasInitPositionY
	};
}
function getRandomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}
function getEndColorStepChange(color) {
	return Math.round((endSegmentColor[color] - initialSegmentColor[color]) / maxNumberOfIterations);
}

// Save / Load Settings
function saveLoadSettings() {
    var settingsStorageType = parseInt($('#settingsStorageType').val(), 10);
    if (this.id === 'loadSettings') {
        loadSettings(settingsStorageType);
    }
    else if(this.id === 'saveSettings') {
        saveSettings(settingsStorageType);
    }
}

function saveSettings(storageType) {
	var name = prompt("Enter the name for this settings.", "");

	if (name == null || name === "") {
		console.log("User cancelled the SaveSettings prompt.");
	} else {
        // Save to LocalStorage
        if (storageType === 0) {
            if (localStorage.getObject('inputParams') == null) {
                localStorage.setObject('inputParams', {})
            }
            var LSInputParams = localStorage.getObject('inputParams');
            LSInputParams[name] = inputParams;
            localStorage.setObject('inputParams', LSInputParams);
        }
        // Save to local disk as file
        else if(storageType === 1) {
            var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(inputParams)),
                element = document.createElement('a');

            element.href = 'data:' + data;
            element.download = name + '.json';
            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();
            document.body.removeChild(element);
        }
        $('#saveLoadResult').text('Settings were successfully saved.');
	}
}
function loadSettings(storageType) {
    // Load from LocalStorage
    if (storageType === 0) {
        var promptText = "Enter the name of settings, you want to load:\n",
            inputParams = localStorage.getObject('inputParams'),
            name;
        if (inputParams == null) {
            alert("There's nothing to load from. Please, save some settings first ;)");
            return false;
        }

        Object.keys(inputParams).forEach(function(key){
            promptText += "- " + key + "\n";
        });

        name = prompt(promptText, "");
        if (name == null || name === "") {
            console.log("User cancelled the LoadSettings prompt.");
            return false;
        } else {
            console.log('ready to load... ' + name);
            if (localStorage.getObject('inputParams') == null) {
                localStorage.setObject('inputParams', {});
            }
            if (inputParams[name] == null) {
                console.log('this settings does not exist... ' + name);
                alert("Settings named " + name + " doesn't exist");
            } else {
                setSettings(inputParams[name]);
                RGBChange();
            }
        }
    }
    // Load from local disk as file
    else if(storageType === 1) {
        loadFile();
        RGBChange();
    }
    $('#saveLoadResult').text('Settings were successfully loaded.');
}
function loadFile() {
    var input, file, fr;

    if (typeof window.FileReader !== 'function') {
        alert("The file API isn't supported on this browser yet.");
        return;
    }

    input = document.getElementById('fileinput');
    if (!input) {
        alert("Um, couldn't find the fileinput element.");
    }
    else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    }
    else if (!input.files[0]) {
        alert("Please select a file before clicking 'Load'");
    }
    else {
        file = input.files[0];
        fr = new FileReader();
        fr.onload = receivedText;
        fr.readAsText(file);
    }
}
function receivedText(e) {
    var lines = e.target.result;
    inputParams = JSON.parse(lines);
    setSettings(inputParams);
}
function setSettings(inputParams) {
	$inputPanel = $('#inputPanel');

	// Canvas
    $inputPanel.find('#canvasWidth').val(inputParams.canvasWidth);
    $inputPanel.find('#canvasHeight').val(inputParams.canvasHeight);
    $inputPanel.find('#canvasInitPositionX').val(inputParams.canvasInitPositionX);
    $inputPanel.find('#canvasInitPositionY').val(inputParams.canvasInitPositionY);

	// Colors
    $inputPanel.find('#R').slider('setValue', inputParams.initialSegmentColor.r, true);
    $inputPanel.find('#G').slider('setValue', inputParams.initialSegmentColor.g, true);
    $inputPanel.find('#B').slider('setValue', inputParams.initialSegmentColor.b, true);
    $inputPanel.find('#R_end').slider('setValue', inputParams.endSegmentColor.r, true);
    $inputPanel.find('#G_end').slider('setValue', inputParams.endSegmentColor.g, true);
    $inputPanel.find('#B_end').slider('setValue', inputParams.endSegmentColor.b, true);
    $inputPanel.find('#R_canvas').slider('setValue', inputParams.canvasBGColor.r, true);
    $inputPanel.find('#G_canvas').slider('setValue', inputParams.canvasBGColor.g, true);
    $inputPanel.find('#B_canvas').slider('setValue', inputParams.canvasBGColor.b, true);
	$inputPanel.find('#colorRStep').val(inputParams.stepChangeColor.r);
	$inputPanel.find('#colorGStep').val(inputParams.stepChangeColor.g);
	$inputPanel.find('#colorBStep').val(inputParams.stepChangeColor.b);
    $inputPanel.find('#useColorStep').prop('checked', inputParams.useColorStep);

	// Size
	$inputPanel.find('#segmentSizeBoundaries').slider('setValue', [inputParams.segmentSizeMin,inputParams.segmentSizeMax], true);
    $inputPanel.find('#initialSegmentSize').val(inputParams.initialSegmentSize);
    $inputPanel.find('#childSizeMultiplier').slider('setValue', [inputParams.sizeMultiplierMin,inputParams.sizeMultiplierMax], true);
    $inputPanel.find('#childSizeSpecialChange').val(inputParams.specialSizeChange);
    $inputPanel.find('#sizeProportion').val(inputParams.sizeProportion);
    $inputPanel.find('#useGenerationRatioForSize').val(inputParams.useGenerationRatioForSize);

    // Angle
    $inputPanel.find('#childAngleDiff').slider('setValue', [inputParams.childAngleDiffMin,inputParams.childAngleDiffMax], true);
    $inputPanel.find('#deflectionAngle').val(inputParams.deflectionAngle);
    $inputPanel.find('#dirTendency').val(inputParams.dirTendency);
    $inputPanel.find('#useGenerationRatioForAngle').val(inputParams.useGenerationRatioForAngle);

    // Global
    $inputPanel.find('#numberOfChildren').slider('setValue', [inputParams.numberOfChildrenMin,inputParams.numberOfChildrenMax], true);
    $inputPanel.find('#maxNumberOfIterations').val(inputParams.maxNumberOfIterations);
	$inputPanel.find('#maxNumberOfSegments').val(inputParams.maxNumberOfSegments);
	$inputPanel.find('#growthSpeed').val(inputParams.growthSpeed);
}
function downloadImg() {
    var name = prompt("Enter the name for current image.", "");

    if (name == null || name === "") {
        console.log("User cancelled the downloadImg prompt.");
    } else {
        var canvas = document.getElementById("canvas"),
            element = document.createElement('a');

        element.href = canvas.toDataURL('png');
        element.download = name + '.png';
        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();
        document.body.removeChild(element);
    }
}

// Creates start and end color from RGB parameters
var RGBChange = function() {
	$('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')');
	$('#RGB_end').css('background', 'rgb('+r_end.getValue()+','+g_end.getValue()+','+b_end.getValue()+')');
    $('#RGB_canvas').css('background', 'rgb('+r_canvas.getValue()+','+g_canvas.getValue()+','+b_canvas.getValue()+')');
};

function clearSaveLoadResult() {
    $('#saveLoadResult').text('');
}

$(document).ready(function() {
	// Initialize elements
	xload();
	
	r = $('#R').slider()
			.on('slide', RGBChange)
			.data('slider');
	g = $('#G').slider()
			.on('slide', RGBChange)
			.data('slider');
	b = $('#B').slider()
			.on('slide', RGBChange)
			.data('slider');

	r_end = $('#R_end').slider()
			.on('slide', RGBChange)
			.data('slider');
	g_end = $('#G_end').slider()
			.on('slide', RGBChange)
			.data('slider');
	b_end = $('#B_end').slider()
			.on('slide', RGBChange)
			.data('slider');

    r_canvas = $('#R_canvas').slider()
        .on('slide', RGBChange)
        .data('slider');
    g_canvas = $('#G_canvas').slider()
        .on('slide', RGBChange)
        .data('slider');
    b_canvas = $('#B_canvas').slider()
        .on('slide', RGBChange)
        .data('slider');
			
	RGBChange();

	$("#childSizeMultiplier").slider({});
	$("#numberOfChildren").slider({});
	$("#childAngleDiff").slider({});
	$('#segmentSizeBoundaries').slider({});

    // Map events to html elements
    $('#submitBtn').on('click', function() {
        xload();
    });

	$('#loadEndColorSteps').on('click', function() {
		// This should be in function
		$inputPanel = $('#inputPanel');
		$inputPanel.find('#colorRStep').val(endColorStepChange.r);
		$inputPanel.find('#colorGStep').val(endColorStepChange.g);
		$inputPanel.find('#colorBStep').val(endColorStepChange.b);
	});

    $('#fileinput').on('change', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        })
        .on('fileselect', function(event, numFiles, label) {
            console.log(numFiles);
            console.log(label);

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
    });

    $('#settingsStorageType').on('change', function() {
        if($(this).val() === '0'){
            $('#selectFile').css('display', 'none');
        }
        else if($(this).val() === '1'){
            $('#selectFile').css('display', 'table');
        }
    });
	
	$('#saveSettings, #loadSettings').on('click', saveLoadSettings);

	$(document).on('change', clearSaveLoadResult);
	$('#submitBtn').on('click', clearSaveLoadResult);
	$('#downloadImg').on('click', downloadImg);
	
});